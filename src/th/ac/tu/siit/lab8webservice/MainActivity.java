package th.ac.tu.siit.lab8webservice;
import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Do not allow changing device orientation
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String,String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, 
				new String[] {"name", "value"}, 
				new int[] {R.id.tvName, R.id.tvValue});
		setListAdapter(adapter);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		//Check if the device is connected to a network
		
		ConnectivityManager mgr = (ConnectivityManager)
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			//Load the data from the web server when it is connected
			
			long current = System.currentTimeMillis();
			//Load only when the time between now and the last update > 5 minutes
			
			if (current - lastUpdate > 5*60*1000) {
				WeatherTask task = new WeatherTask(this);
				task.execute("http://cholwich.org/bangkok.json");
			}
		}
		else {
			Toast t = Toast.makeText(this, 
					"No Internet Connectivity on this device. Check your setting", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	String province = "Bangkok";
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_refresh:
			
			ConnectivityManager mgr = (ConnectivityManager)
			getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				//Load the data from the web server when it is connected
		
				long current = System.currentTimeMillis();
				//Load only when the time between now and the last update > 5 minutes
		
				if (current - lastUpdate > 3*60*1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/bangkok.json");
				}
			}
			else {
				Toast t = Toast.makeText(this, 
				"No Internet Connectivity on this device. Check your setting", 
				Toast.LENGTH_LONG);
				t.show();
			}
			return true;
			
		case R.id.action_bangkok:
			ConnectivityManager mgr1 = (ConnectivityManager)
			getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info1 = mgr1.getActiveNetworkInfo();
			if (info1 != null && info1.isConnected()) {
				//Load the data from the web server when it is connected
		
				long current = System.currentTimeMillis();
				//Load only when the time between now and the last update > 5 minutes
		
				if (current - lastUpdate > 1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/bangkok.json");
					Toast t = Toast.makeText(this, 
							"Bangkok", 
							Toast.LENGTH_LONG);
							t.show();
					province = "Bangkok";
				}
			}
			else {
				Toast t = Toast.makeText(this, 
				"No Internet Connectivity on this device. Check your setting", 
				Toast.LENGTH_LONG);
				t.show();
			}
			return true;
			
		case R.id.action_nonthaburi:
			ConnectivityManager mgr2 = (ConnectivityManager)
			getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info2 = mgr2.getActiveNetworkInfo();
			if (info2 != null && info2.isConnected()) {
				//Load the data from the web server when it is connected
		
				long current = System.currentTimeMillis();
				//Load only when the time between now and the last update > 5 minutes
		
				if (current - lastUpdate > 1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/nonthaburi.json");
					Toast t = Toast.makeText(this, 
							"Nonthaburi", 
							Toast.LENGTH_LONG);
							t.show();
					province = "Nonthaburi";
				}
			}
			else {
				Toast t = Toast.makeText(this, 
				"No Internet Connectivity on this device. Check your setting", 
				Toast.LENGTH_LONG);
				t.show();
			}
			return true;
			
		case R.id.action_pathumthani:
			ConnectivityManager mgr3 = (ConnectivityManager)
			getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info3 = mgr3.getActiveNetworkInfo();
			if (info3 != null && info3.isConnected()) {
				//Load the data from the web server when it is connected
		
				long current = System.currentTimeMillis();
				//Load only when the time between now and the last update > 5 minutes
		
				if (current - lastUpdate > 1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/pathumthani.json");
					Toast t = Toast.makeText(this, 
							"Pathum Thani", 
							Toast.LENGTH_LONG);
							t.show();
					province = "Pathum Thani";
				}
			}
			else {
				Toast t = Toast.makeText(this, 
				"No Internet Connectivity on this device. Check your setting", 
				Toast.LENGTH_LONG);
				t.show();
			}
			return true;
			
			
		}
		return super.onOptionsItemSelected(item);
	}


	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String,String> record;
		ProgressDialog dialog;
		
		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}
		
		//Executed under UI thread
		//Called before the task starts
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		//Executed under UI thread
		//Called after the task finished
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), 
					result, Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			
			//Update the title bar of the activity
			
			setTitle(province+" Weather");
		}

		//Executed under Background thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				
				//Use the first parameter as a URL
				URL url = new URL(params[0]);
				
				//Connect to the URL
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				
				//Read data from the URL
				http.setDoInput(true);
				http.connect();
				
				response = http.getResponseCode(); //ResponseCode in Network
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(http.getInputStream()));
					while((line = in.readLine()) != null) {
						buffer.append(line); //String builder
					}
					
					// JSON = JavaScript Object Notation
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String,String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp")-273.15;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp));
					list.add(record);
					
					//"weather" : [ { "description": :.....", } ],
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String desc = w0.getString("description");
					record = new HashMap<String, String>();
					record.put("name", "Description");
					record.put("value", desc);
					list.add(record);
					
					//Pressure
					
					JSONObject jpress = json.getJSONObject("main");
					record = new HashMap<String, String>();
					record.put("name", "Pressure");
					double press1 = jpress.getDouble("pressure");
					record.put("value", String.format(Locale.getDefault(), " %.0f hpa", press1));
					list.add(record);
					
					//Humidity
					
					JSONObject jhumid = json.getJSONObject("main");
					record = new HashMap<String, String>();
					record.put("name", "Humidity");
					double humid = jhumid.getDouble("humidity");
					record.put("value", String.format(Locale.getDefault(), " %.0f ", humid));
					list.add(record);
					
					//TempMin
					
					JSONObject jTMin = json.getJSONObject("main");
					record = new HashMap<String, String>();
					record.put("name", "Minimum Temperature");
					double tmin = jTMin.getDouble("temp_min")-273.15;
					record.put("value", String.format(Locale.getDefault(), " %.2f degree celsius ", tmin));
					list.add(record);
					
					//TempMax
					
					JSONObject jTMax = json.getJSONObject("main");
					record = new HashMap<String, String>();
					record.put("name", "Maximum Temperature");
					double tmax = jTMax.getDouble("temp_max")-273.15;
					record.put("value", String.format(Locale.getDefault(), " %.2f degree celsius ", tmax));
					list.add(record);
					
					//Wind speed
					JSONObject jWindSp = json.getJSONObject("wind");
					record = new HashMap<String, String>();
					record.put("name", "Wind Speed");
					double speed = jWindSp.getDouble("speed");
					record.put("value", String.format(Locale.getDefault(), " %.1f m/s ", speed));
					list.add(record);
					
					//Wind deg
					JSONObject jWindDeg = json.getJSONObject("wind");
					record = new HashMap<String, String>();
					record.put("name", "Wind Degree");
					double deg = jWindDeg.getDouble("deg");
					record.put("value", String.format(Locale.getDefault(), " %.0f ", deg));
					list.add(record);
					
					return "Finished Loading Weather Data";
				}
				else {
					return "Error "+response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}	
		}
		
		
	}

}

